@extends('layout')
@section('title', $title)
@section('style')
@stop
@section('body')
    <blockquote class="layui-elem-quote">{{$title}}</blockquote>
    <form class="layui-form" method="post" action="{{route('system.config')}}">
        <div class="layui-form-item">
            <div class="layui-tab">
                <ul class="layui-tab-title">
                    <li class="layui-this b">基本参数配置</li>
                    <li class="b">微信参数配置</li>
                </ul>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        @include('system._base_config')
                    </div>
                    <div class="layui-tab-item">
                        @include('system._weixin_config')
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-row mt5 tc">
            <div class="layui-col-md4">
                {{csrf_field()}}
                <button class="layui-btn" lay-submit lay-filter="wxConfig">保存</button>
            </div>
        </div>
    </form>
@stop
@section('script')
    <script>
        layui.use(['table', 'form', 'element'], function () {
            var table = layui.table;
            var form = layui.form;
            var element = layui.element;
            //监听提交
            form.on('submit(wxConfig)', function (data) {
                $.post(data.form.action, data.field, function (rev) {
                    if (rev.status === 0) {
                        layer.msg(rev.msg);
                    } else {
                        layer.msg(rev.msg);
                    }
                });
                return false;
            });
        });
    </script>
@stop