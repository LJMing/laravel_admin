<div class="layui-row">
    <div class="layui-col-md4">
        <label class="layui-form-label">数据库备份路径</label>
        <div class="layui-input-block">
            <input type="text" name="back_db_path" lay-verType="tips"
                   placeholder="请输入" value="{{$item['back_db_path'] or ''}}"
                   autocomplete="off" class="layui-input">
        </div>
    </div>
</div>
<div class="layui-row mt5">
    <div class="layui-col-md4">
        <label class="layui-form-label">站点备份路径</label>
        <div class="layui-input-block">
            <input type="text" name="back_site_path" lay-verType="tips"
                   placeholder="请输入" value="{{$item['back_site_path'] or ''}}"
                   autocomplete="off" class="layui-input">
        </div>
    </div>
</div>
