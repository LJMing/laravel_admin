@extends('layout')
@section('title', $title)
@section('style')
@stop
@section('body')
    <blockquote class="layui-elem-quote">{{$title}}</blockquote>
    <ul id="main"></ul>
@stop
@section('script')
    <script>
        $(function () {
            $(document).keyup(function (event) {
                if (event.keyCode === 13) {
                    $("#submit").trigger("click");
                }
            });
            layui.use(['tree', 'layer'], function () {
                var tree = layui.tree;
                var layer = layui.layer;
                var option = {
                    elem: '#main' //传入元素选择器
                    , nodes: []
                    , click: function (node) {
                        if (typeof node.md5 !== 'undefined') {
                            layer.confirm('您确定要下载吗?', {btn: ['确定', '取消']}, function (index) {
                                window.location.href = "{{route('system.download')}}?md5=" + node.md5;
                                layer.close(index);
                            }, function (index) {
                                layer.close(index);
                            });
                        }
                    }
                };
                $.post('{{route('system.back_site')}}', function (rev) {
                    if (rev.status === 0) {
                        option.nodes = rev.data;
                        tree(option);
                    }
                });
            });
        });
    </script>
@stop